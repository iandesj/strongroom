from strongroom.helper import Helper
from strongroom.room import Room
from strongroom.shelf import Shelf
from strongroom.config import Config


class Cli:

    def __init__(self, options={}, args=[]):
        self.config = Config()
        self.opts = options
        self.args = args

    def exec_action(self):
        if self.opts.generate:
            self.opts.password = Helper.generate_pass(policy=self.opts.generate[0],
                                                      length=self.opts.generate[1])

        if self.opts.new:
            if self.opts.new.lower() == 'shelf':
                try:
                    shelf = Shelf(room_name=self.opts.room_name,
                                  account=self.opts.acct_name,
                                  login=self.opts.login,
                                  password=self.opts.password,
                                  website=self.opts.website)
                    Helper.validate_shelf(shelf)
                    if shelf.create():
                        print('Shelf %s created!' % self.opts.acct_name)
                except AssertionError:
                    print("Must specify room name with '-r' or '--room=NAME' argument")
                    print("Must specify account name with '-a' or '--account=ACCOUNT' argument")
                    print("Must specify login name with '-l' or '--login=LOGIN' argument")
                    print("Must specify password with '-p' or '--password=PASSWORD' argument")
                    print("Optionally specify website with '-w' or '--website=WEBSITE' option")
            elif self.opts.new.lower() == 'room':
                try:
                    room = Room(name=self.opts.room_name)
                    Helper.validate_room(room)
                    if room.create():
                        print('Room %s created!' % self.opts.room_name)
                except AssertionError:
                    print("Must specify room name with '-r' or '--room=NAME' option")
            else:
                print("Must specify option 'shelf' or 'room' for creation.")

        if self.opts.update:
            if self.opts.is_shelf:
                try:
                    shelf = Shelf(room_name=self.opts.room_name,
                                  account=self.opts.acct_name,
                                  login=self.opts.login if self.opts.login else "",
                                  password=self.opts.password if self.opts.password else "",
                                  website=self.opts.website if self.opts.website else "")
                    Helper.validate_shelf(shelf)
                    if shelf.update():
                        print('Shelf %s updated!' % self.opts.acct_name)
                except AssertionError:
                    print("Must specify room name with '-r' or '--room=NAME' argument")
                    print("Must specify account name with '-a' or '--account=ACCOUNT' argument")
                    print("Optionally specify login name with '-l' or '--login=LOGIN' option")
                    print("Optionally specify password with '-p' or '--password=PASSWORD' option")
                    print("Optionally specify website with '-w' or '--website=WEBSITE' option")
            else:
                print("Must specify '-s' or '--shelf' flag for update")

        if self.opts.delete:
            if self.opts.is_shelf:
                try:
                    shelf = Shelf(room_name=self.opts.room_name,
                                  account=self.opts.acct_name,
                                  login=self.opts.login if self.opts.login else "",
                                  password=self.opts.password if self.opts.password else "",
                                  website=self.opts.website if self.opts.website else "")
                    Helper.validate_shelf(shelf)
                    if shelf.delete():
                        print('Shelf %s deleted!' % self.opts.acct_name)
                except AssertionError:
                    print("Must specify room name with '-r' or '--room=NAME' argument")
                    print("Must specify account name with '-a' or '--account=ACCOUNT' argument")

            elif (not self.opts.is_shelf) and self.opts.room_name:
                room = Room(name=self.opts.room_name)
                if room.delete():
                    print('Room %s deleted!' % self.opts.room_name)
            else:
                print("For deleting a shelf:")
                print("\tMust specify '-s' or '--shelf' flag")
                print("\tMust specify '-r' or '--room=ROOM' argument")
                print("\tMust specify '-a' or '--account=ACCOUNT' argument")
                print("For deleting a room:")
                print("\tMust specify '-r' or '--room=ROOM' argument")

        if self.opts.copy_to_cb:
            try:
                shelf = Shelf(room_name=self.opts.room_name,
                              account=self.opts.acct_name,
                              login=self.opts.login if self.opts.login else "",
                              password=self.opts.password if self.opts.password else "",
                              website=self.opts.website if self.opts.website else "")
                Helper.validate_shelf(shelf)
                shelf.copy_password()
                print('Shelf %s password copied to clipboard!' % self.opts.acct_name)
            except AssertionError:
                print("Must specify room name with '-r' or '--room=NAME' argument")
                print("Must specify account name with '-a' or '--account=ACCOUNT' argument")
