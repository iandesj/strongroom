import datetime
import string
import random
import requests


class Helper:

    @staticmethod
    def validate_shelf(shelf):
        assert isinstance(shelf.room_name, str)
        assert isinstance(shelf.account, str)
        assert isinstance(shelf.login, str)
        assert isinstance(shelf.password, str)

    @staticmethod
    def validate_room(room):
        assert isinstance(room.name, str)

    @staticmethod
    def timestamp():
        return '{:%Y-%m-%d %H:%M:%S}'.format(datetime.datetime.now())

    @staticmethod
    def generate_pass(policy='', length=21):
        """Function to generate a password"""
        policy = str(policy)
        length = int(length)
        choices = ''
        if 'w' in policy:
            return Helper.generate_pass_from_words(length=length)
        if 'l' in policy:
            choices += string.ascii_lowercase
        if 'u' in policy:
            choices += string.ascii_uppercase
        if 'n' in policy:
            choices += string.digits
        if 's' in policy:
            choices += string.punctuation
        if policy is '':
            return Helper.generate_pass_default(length)

        return ''.join(random.choice(
            choices) for _ in range(length))

    @staticmethod
    def generate_pass_default(length):
        length = int(length)
        choices = ''
        choices += string.ascii_lowercase
        choices += string.ascii_uppercase
        choices += string.digits
        choices += string.punctuation

        pw = ''.join(random.choice(
            choices) for _ in range(length))
        try:
            if not any((c in set(string.ascii_lowercase) for c in pw)):
                assert False
            if not any((c in set(string.ascii_uppercase) for c in pw)):
                assert False
            if not any((c in set(string.digits) for c in pw)):
                assert False
            if not any((c in set(string.punctuation) for c in pw)):
                assert False
            return pw
        except AssertionError:
            return Helper.generate_pass_default(length)

    @staticmethod
    def generate_pass_from_words(length=21):
        """Function to generate a password from randomly selected words"""
        word_site = "http://svnweb.freebsd.org/csrg/share/dict/words" \
                    "?view=co&content-type=text/plain"

        response = requests.get(word_site)
        WORDS = response.content.decode('utf-8').splitlines()
        password = ''
        while len(password) <= length:
            password += ''.join(random.choice(WORDS))

        return password[:length].strip()
