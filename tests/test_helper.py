import datetime
import string
import pytest
from strongroom.helper import Helper
from strongroom.shelf import Shelf
from strongroom.room import Room


class HelperTest:

    def test_validate_shelf_valid(self):
        shelf = Shelf(room_name='test', account='test',
                      login='test', password='test')
        Helper.validate_shelf(shelf)

    def test_validate_shelf_invalid_room(self):
        shelf = Shelf(account='test',
                      login='test', password='test')
        with pytest.raises(AssertionError):
            Helper.validate_shelf(shelf)

    def test_validate_shelf_invalid_account(self):
        shelf = Shelf(room_name='test',
                      login='test', password='test')
        with pytest.raises(AssertionError):
            Helper.validate_shelf(shelf)

    def test_validate_shelf_invalid_login(self):
        shelf = Shelf(room_name='test', account='test',
                      password='test')
        with pytest.raises(AssertionError):
            Helper.validate_shelf(shelf)

    def test_validate_shelf_invalid_password(self):
        shelf = Shelf(room_name='test', account='test',
                      login='test')
        with pytest.raises(AssertionError):
            Helper.validate_shelf(shelf)

    def test_validate_room(self):
        room = Room('test')
        Helper.validate_room(room)

    def test_validate_room_invalid_room_name(self):
        room = Room()
        with pytest.raises(AssertionError):
            Helper.validate_room(room)

    def test_timestamp(self):
        timestamp = Helper.timestamp()
        assert datetime.datetime.strptime(timestamp, '%Y-%m-%d %H:%M:%S')

    def test_generate_pass_default_policy(self):
        pw = Helper.generate_pass()
        assert len(pw) == 21
        if not any((c in set(string.ascii_lowercase) for c in pw)):
            assert False
        if not any((c in set(string.ascii_uppercase) for c in pw)):
            assert False
        if not any((c in set(string.digits) for c in pw)):
            assert False
        if not any((c in set(string.punctuation) for c in pw)):
            assert False

    def test_generate_pass_custom_length(self):
        pw = Helper.generate_pass(length=32)
        assert len(pw) == 32
        if not any((c in set(string.ascii_lowercase) for c in pw)):
            assert False
        if not any((c in set(string.ascii_uppercase) for c in pw)):
            assert False
        if not any((c in set(string.digits) for c in pw)):
            assert False
        if not any((c in set(string.punctuation) for c in pw)):
            assert False

    def test_generate_pass_lower_policy(self):
        pw = Helper.generate_pass(policy='l')
        assert len(pw) == 21
        if not any((c in set(string.ascii_lowercase) for c in pw)):
            assert False
        if any((c in set(string.ascii_uppercase) for c in pw)):
            assert False
        if any((c in set(string.digits) for c in pw)):
            assert False
        if any((c in set(string.punctuation) for c in pw)):
            assert False

    def test_generate_pass_upper_policy(self):
        pw = Helper.generate_pass(policy='u')
        assert len(pw) == 21
        if not any((c in set(string.ascii_uppercase) for c in pw)):
            assert False
        if any((c in set(string.ascii_lowercase) for c in pw)):
            assert False
        if any((c in set(string.digits) for c in pw)):
            assert False
        if any((c in set(string.punctuation) for c in pw)):
            assert False

    def test_generate_pass_digits_policy(self):
        pw = Helper.generate_pass(policy='n')
        assert len(pw) == 21
        if not any((c in set(string.digits) for c in pw)):
            assert False
        if any((c in set(string.ascii_uppercase) for c in pw)):
            assert False
        if any((c in set(string.ascii_lowercase) for c in pw)):
            assert False
        if any((c in set(string.punctuation) for c in pw)):
            assert False

    def test_generate_pass_special_chars_policy(self):
        pw = Helper.generate_pass(policy='s')
        assert len(pw) == 21
        if not any((c in set(string.punctuation) for c in pw)):
            assert False
        if any((c in set(string.digits) for c in pw)):
            assert False
        if any((c in set(string.ascii_uppercase) for c in pw)):
            assert False
        if any((c in set(string.ascii_lowercase) for c in pw)):
            assert False

    def test_generate_pass_from_words(self):
        pw = Helper.generate_pass(policy='w', length=32)
        assert len(pw) == 32
