Strongroom
==========

A wonderful and exciting command line utility for creating encrypted account credentials files stored somewhere in the cloud [AWS S3].
AES256 Encryption with 32 byte long key

### TODO
- [  ] Add `--download` option
- [  ] Refactor class design
- [  ] Figure out module installation
- [  ] PEP8 Compliant
- [  ] Write tests
- [  ] Refactor cli interface logic tree
- [  ] Add `--force` when generating **NEW** passwords
- [  ] Add ability to delete an entire room.
- [  ] Add `--force` when deleting a shelf and/or room
- [  ] Add `--configure` option to build `~/.strongroomrc`
- [  ] Add stdin prompt if there is no key in `~/.strongroomrc`
- [  ] Pick up profile from `~/.strongroomrc` if there is one
- [  ] Automatically rotate keys if older than X days old, configurable in dotfile
- [  ] Add good documentation for classes and methods
- [  ] Finish README
