import csv
import multiprocessing
import xerox
from io import StringIO
from threading import Timer
from strongroom.room import Room
from strongroom.helper import Helper


class Shelf:

    def __init__(self, room_name=None, account=None,
                 login=None, password=None, website=None):
        self.room = Room(room_name)
        self.room_name = room_name
        self.account = account
        self.login = login
        self.password = password
        self.website = website

    def create(self):
        try:
            self.validate_unique()
            updated_room = StringIO(self.room.retrieve().getvalue())
            csv.writer(updated_room, delimiter=' ', quotechar='|',
                       quoting=csv.QUOTE_MINIMAL).writerow(
                            [
                                self.account, self.login, self.password,
                                self.website, Helper.timestamp()
                            ])
            return self.room.update(
                updated_room=updated_room)
        except ValueError as e:
            print("Could not create Shelf. %s" % e)

    def update(self):
        self.validate_exists()
        updated_room = StringIO()
        writer = csv.writer(updated_room, delimiter=' ',
                            quotechar='|', quoting=csv.QUOTE_MINIMAL)
        for shelf in csv.reader(self.room.retrieve().getvalue().splitlines(),
                                delimiter=' ', quotechar='|'):
            if shelf[0].upper().strip() == self.account.upper().strip():
                writer.writerow(
                    [
                        self.account,
                        self.login if self.login else shelf[1],
                        self.password if self.password else shelf[2],
                        self.website if self.website else shelf[3],
                        Helper.timestamp()
                    ])
            else:
                writer.writerow(shelf)
        return self.room.update(
            updated_room=updated_room)

    def delete(self):
        updated_room = StringIO()
        writer = csv.writer(updated_room, delimiter=' ',
                            quotechar='|', quoting=csv.QUOTE_MINIMAL)
        for shelf in csv.reader(self.room.retrieve().getvalue().splitlines(),
                                delimiter=' ', quotechar='|'):
            if shelf[0].upper().strip() != self.account.upper().strip():
                writer.writerow(shelf)
        return self.room.update(
            updated_room=updated_room)

    def copy_password(self):
        try:
            self.validate_exists()
            shelves = self.room.retrieve().getvalue().splitlines()
            old_cb = None
            for shelf in csv.reader(shelves,
                                    delimiter=' ', quotechar='|'):
                if shelf[0].upper().strip() == self.account.upper().strip():
                    old_cb = xerox.paste()  # Retain old clipboard buffer
                    xerox.copy(shelf[2])
                    break
            restore_clipboard = multiprocessing.Process(
                                target=xerox.copy, args=(old_cb,))
            timer = Timer(30.0, restore_clipboard.start)
            timer.start()
        except ValueError as e:
            print("Password could not be copied. %s" % e)

    def validate_unique(self):
        for shelf in csv.reader(self.room.retrieve().getvalue().splitlines(),
                                delimiter=' ', quotechar='|'):
            if shelf[0].upper().strip() == self.account.upper().strip():
                raise ValueError("Account '%s' not unique." % self.account)
                break

    def validate_exists(self):
        shelves = self.room.retrieve().getvalue().splitlines()
        counter = 0
        if shelves:
            for shelf in csv.reader(shelves,
                                    delimiter=' ', quotechar='|'):
                counter += 1
                if shelf[0].upper().strip() == self.account.upper().strip():
                    break
                if counter == len(shelves):
                    raise ValueError("Account '%s' does not exist."
                                     % self.account)
        else:
            raise ValueError("Account '%s' does not exist." % self.account)
