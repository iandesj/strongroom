from Crypto import Random
from Crypto.Cipher import AES


class Encryption:

    @staticmethod
    def pad(s):
        return s.encode() + b"\0" * (AES.block_size - len(s) % AES.block_size)

    @staticmethod
    def encrypt(message, key, key_size=256):
        message = Encryption.pad(message)
        iv = Random.new().read(AES.block_size)
        cipher = AES.new(key, AES.MODE_CBC, iv)
        return iv + cipher.encrypt(message)

    @staticmethod
    def decrypt(ciphertext, key):
        iv = ciphertext[:AES.block_size]
        cipher = AES.new(key, AES.MODE_CBC, iv)
        plaintext = cipher.decrypt(ciphertext[AES.block_size:])
        return plaintext.rstrip(b"\0")

    @staticmethod
    def encrypt_file(file_name, key):
        plaintext = file_name.getvalue()
        enc = Encryption.encrypt(plaintext, key)
        return enc

    @staticmethod
    def decrypt_file(file_name, key):
        ciphertext = file_name
        dec = Encryption.decrypt(ciphertext, key)
        return dec
