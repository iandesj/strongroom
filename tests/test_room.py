import boto3
import botocore
import pytest
import os
from io import StringIO
from moto import mock_s3
from strongroom.room import Room
from strongroom.config import Config

BUCKET = 'TEST_BUCKET'
ROOM = 'TEST_ROOM'


class RoomTest:

    @mock_s3
    def setup(self):
        os.environ['STRONG_KEY'] = 'passwordpasswordpasswordpassword'
        os.environ['STRONG_BUCKET'] = BUCKET
        Config().config_strategies()
        client = boto3.client('s3')
        client.create_bucket(Bucket=BUCKET)
        client.put_object(Bucket=BUCKET,
                          Key=ROOM)
        return client

    @mock_s3
    def test_create(self):
        client = self.setup()
        room = Room(ROOM)
        room.create()

        assert client.list_objects(
                Bucket=BUCKET,
                Prefix=ROOM)['Contents'][0]['Key'] == ROOM

    @mock_s3
    def test_retrieve(self):
        self.setup()

        # Setup Room test data
        room = Room(ROOM)
        room.create()

        assert room.retrieve()

    @mock_s3
    def test_retrieve_fail(self):
        os.environ['STRONG_KEY'] = 'passwordpasswordpasswordpassword'
        os.environ['STRONG_BUCKET'] = BUCKET
        Config().config_strategies()
        client = boto3.client('s3')
        client.create_bucket(Bucket=BUCKET)

        with pytest.raises(botocore.exceptions.ClientError):
            # Setup Room test data
            room = Room(ROOM)
            room.retrieve()

    @mock_s3
    def test_update(self):
        client = self.setup()

        # Setup updated Room test data
        updated_room = StringIO()
        updated_room.write('test test test test test')

        # Setup Room object for test
        room = Room(ROOM)
        room.update(updated_room=updated_room)

        assert client.list_objects(
                Bucket=BUCKET,
                Prefix=ROOM)['Contents'][0]['Key'] == ROOM

    @mock_s3
    def test_delete(self):
        client = self.setup()
        client.delete_object(Bucket=BUCKET,
                             Key=ROOM)

        # Setup Room test data
        room = Room(name=ROOM)
        room.create()
        room.delete()

        assert 'Contents' not in client.list_objects(Bucket=BUCKET,
                                                     Prefix=ROOM)
