import boto3
import csv
import logging
import os
from strongroom.encrypt import Encryption
from botocore.exceptions import ClientError
from io import StringIO

HEADER_ROW = ['Account', 'Login', 'Password', 'Website', 'TimeStamp']


class Room:

    def __init__(self, name=None):
        self.name = name
        self.room = None

    def create(self):
        self.room = StringIO()
        try:
            room_writer = csv.writer(
                            self.room, delimiter=' ',
                            quotechar='|', quoting=csv.QUOTE_MINIMAL
            )
            room_writer.writerow(HEADER_ROW)
            return boto3.client('s3').put_object(
                                Bucket=os.environ['STRONG_BUCKET'],
                                Key=self.name,
                                Body=Encryption.encrypt_file(
                                                    self.room,
                                                    os.environ['STRONG_KEY']
                                )
            )['ResponseMetadata']['HTTPStatusCode'] == 200
        except ClientError as e:
            logging.error("Error: %s \n" % e)
            print('Room could not be created!')

    def retrieve(self):
        if self.room:
            return self.room
        try:
            self.room = boto3.client('s3').get_object(
                            Bucket=os.environ['STRONG_BUCKET'],
                            Key=self.name)['Body'].read()
            self.room = StringIO(Encryption.decrypt_file(
                                self.room,
                                os.environ['STRONG_KEY']
            ).decode('utf-8'))
            return self.room
        except ClientError as e:
            logging.error("Error: %s \n" % e)
            print('Room could not be retrieved!')
            raise e

    def update(self, updated_room=None):
        try:
            return boto3.client('s3').put_object(
                                Bucket=os.environ['STRONG_BUCKET'],
                                Key=self.name,
                                Body=Encryption.encrypt_file(
                                                    updated_room,
                                                    os.environ['STRONG_KEY']
                                )
            )['ResponseMetadata']['HTTPStatusCode'] == 200
        except ClientError as e:
            logging.error("Error: %s \n" % e)
            print('Room could not be updated!')

    def delete(self):
        try:
            return boto3.client('s3').delete_object(
                                Bucket=os.environ['STRONG_BUCKET'],
                                Key=self.name
            )['ResponseMetadata']['HTTPStatusCode'] == 204
        except ClientError as e:
            logging.error("Error: %s \n" % e)
            print('Room could not be deleted!')
