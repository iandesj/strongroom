import csv
import getpass
import os
from io import StringIO


class Config:

    def __init__(self, rc_path=None):
        self.rc_path = rc_path
        self.user_key = None

    def create(self):
        #  TODO: add config creation
        pass

    def config_strategies(self):
        try:
            os.environ['STRONG_KEY']
            os.environ['STRONG_BUCKET']
        except KeyError:
            self.from_file()
        except IOError as e:
            self.from_user_prompt()

    def from_file(self):
        path = '/'.join([self.rc_path, '.strongroomrc']) if self.rc_path else '/'.join([os.environ['HOME'],
                                                           '.strongroomrc'])
        if not os.path.isfile(path):
            raise IOError("File path does not exist")
        with open(path, 'r') as configfile:
            config = StringIO(configfile.read())
            reader = csv.reader(config, delimiter='=')
            for line in reader:
                os.environ[line[0]] = line[1]

            if 'STRONG_KEY' not in config.getvalue():
                raise KeyError("'STRONG_KEY' is not an env variable.")

    def from_user_prompt(self):
        self.user_key = getpass.getpass()
        os.environ['STRONG_KEY'] = self.user_key
