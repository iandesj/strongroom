import boto3
import pytest
from io import StringIO
from moto import mock_s3
from strongroom.shelf import Shelf
from strongroom.config import Config
from unittest.mock import MagicMock

BUCKET = 'TEST_BUCKET'
ROOM = 'TEST_ROOM'


class ShelfTest:

    @mock_s3
    def s3_setup(self):
        Config()
        client = boto3.client('s3')
        client.create_bucket(Bucket=BUCKET)
        client.put_object(Bucket=BUCKET,
                          Key=ROOM)
        return client

    @mock_s3
    def test_create(self):
        Config()
        self.s3_setup()
        shelf = Shelf(room_name=ROOM, account='testing1',
                      login='test_guy', password='password',
                      website='example.org')
        ret = StringIO("testing test_guy password " +
                       "example.org |2016-08-03 23:51:42|\r\n")
        shelf.room.retrieve = MagicMock(return_value=ret)
        shelf.room.update = MagicMock(return_value=True)
        assert shelf.create()

    @mock_s3
    def test_create_fail(self):
        Config()
        self.s3_setup()
        shelf = Shelf(room_name=ROOM, account='testing2',
                      login='test_guy', password='password',
                      website='example.org')
        shelf.room.retrieve = MagicMock(
                                side_effect=Exception())
        with pytest.raises(Exception):
            shelf.create()
            shelf.room.retrieve.assert_called_with()

    @mock_s3
    def test_update(self):
        Config()
        self.s3_setup()
        shelf = Shelf(room_name=ROOM, account='testing3',
                      login='test_guy', password='password',
                      website='example.org')
        ret = StringIO("testing3 test_guy password " +
                       "example.org |2016-08-03 23:51:42|\r\n")
        shelf.room.retrieve = MagicMock(return_value=ret)
        shelf.room.update = MagicMock(return_value=True)
        assert shelf.update()
        shelf.room.retrieve.assert_called_with()

    @mock_s3
    def test_update_fail(self):
        Config()
        self.s3_setup()
        shelf = Shelf(room_name=ROOM, account='testing4',
                      login='test_gal', password='new_password',
                      website='example.net')
        shelf.room.retrieve = MagicMock(
                                side_effect=Exception())
        with pytest.raises(Exception):
            shelf.update()
            shelf.room.retrieve.assert_called_with()

    @mock_s3
    def test_delete(self):
        Config()
        self.s3_setup()
        shelf = Shelf(room_name=ROOM, account='testing')
        ret = StringIO("testing test_guy password " +
                       "example.org |2016-08-03 23:51:42|\r\n")
        shelf.room.retrieve = MagicMock(return_value=ret)
        shelf.room.update = MagicMock(return_value=True)
        assert shelf.delete()
        shelf.room.retrieve.assert_called_with()

    @mock_s3
    def test_delete_fail(self):
        Config()
        self.s3_setup()
        shelf = Shelf(room_name=ROOM, account='testing')
        shelf.room.retrieve = MagicMock(
                                side_effect=Exception())
        with pytest.raises(Exception):
            shelf.update()
            shelf.room.retrieve.assert_called_with()
