#!/usr/bin/env python3

from setuptools import setup

with open('requirements.txt') as f:
    required = f.read().splitlines()

setup(
    name='strongroom',
    version='0.1.1',
    description='A cli driven password manager.',
    author='Ian DesJardins',
    url='https://gitlab.com/iandesj/strongroom',
    license='MIT',
    packages=['strongroom'],
    install_requires=required,
    entry_points={
        'console_scripts': [
            'strongroom=strongroom.__main__:main'
        ],
    }
)
