from .cli import Cli
from optparse import OptionParser

parser = OptionParser()
parser.add_option("-n", "--new", dest="new",
                  help="Create new: room or shelf")
parser.add_option("-u", "--update", action="store_true", dest="update",
                  help="Update a shelf")
parser.add_option("-v", "--view", action="store_true", dest="view",
                  help="View all shelves tied to a room")
parser.add_option("-r", "--room", dest="room_name",
                  help="Specify the room name to operate on")
parser.add_option("-s", "--shelf", action="store_true", dest="is_shelf",
                  help="Specify that you are operating on a shelf")
parser.add_option("-a", "--account", dest="acct_name",
                  help="Specified account name for a shelf")
parser.add_option("-l", "--login", dest="login",
                  help="Login or username for an shelf")
parser.add_option("-p", "--password", dest="password",
                  help="Password for a shelf")
parser.add_option("-w", "--website", dest="website", default="",
                  help="Website for the account on the shelf")
parser.add_option("-g", "--generate", dest="generate", nargs=2,
                  help="Requires <POLICY> <LENGTH>")
parser.add_option("-d", "--delete", action="store_true", dest="delete",
                  help="Delete the room or account")
parser.add_option(
            "-c", "--copy", action="store_true", dest="copy_to_cb",
            help="Copy the password for the specified account to clipboard")
parser.add_option("-o", "--open", action="store_true", dest="open_site",
                  help="Open the website tied to the shelf account")

(options, args) = parser.parse_args()


def main():
    cli = Cli(options=options, args=args)
    cli.config.config_strategies()
    cli.exec_action()

if __name__ == '__main__':
    main()
